## Suitor

Easily publish a tor .onion service from with a go application.

# Example

```
func main() {
	// make a new private key for the tor hidden service
	priv, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		log.Fatal(err)
	}

	// start listening
	l, err := suitor.Listen(priv, 80, 60*time.Second)
	if err != nil {
		log.Fatal(err)
	}

	// print our newly created onion address so we can test
	fmt.Printf("address is %v\n", l.Addr())

	// use the listener for an HTTP server
	http.Serve(l, http.HandlerFunc(handle))
}

func handle(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello world via a tor service wrapped in golang")
}
```
