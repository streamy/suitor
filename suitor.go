package suitor

import (
	"bufio"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/x509"
	"encoding/asn1"
	"encoding/base32"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"math/big"
	"net"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"
)

var ErrTimeout = errors.New("Timeout connecting to the tor network")

func Listen(key *rsa.PrivateKey, port int, timeout time.Duration) (net.Listener, error) {
	address, err := rsaKeyToOnionAddress(key.PublicKey)
	if err != nil {
		return nil, err
	}

	dir, err := ioutil.TempDir("", "togor-")
	if err != nil {
		return nil, err
	}

	hsDir := filepath.Join(dir, "hs")
	_ = os.Mkdir(hsDir, 0700)

	f, err := os.OpenFile(filepath.Join(hsDir, "private_key"), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		_ = os.RemoveAll(dir)
		return nil, err
	}
	ser := x509.MarshalPKCS1PrivateKey(key)
	err = pem.Encode(f, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: ser})
	f.Close()
	if err != nil {
		_ = os.RemoveAll(dir)
		return nil, err
	}

	err = ioutil.WriteFile(filepath.Join(hsDir, "hostname"), []byte(fmt.Sprintf("%s\n", address)), 0600)
	if err != nil {
		_ = os.RemoveAll(dir)
		return nil, err
	}

	l, err := net.Listen("tcp", ":0")
	if err != nil {
		_ = os.RemoveAll(dir)
		return nil, err
	}
	tl := l.(*net.TCPListener)
	ta := tl.Addr().(*net.TCPAddr)
	tcpPort := ta.Port

	cmd := exec.Command(
		"tor",
		"-f", "/dev/null",
		"--ignore-missing-torrc",
		"--SocksPort", "0",
		"--HiddenServiceDir", hsDir,
		"--HiddenServicePort", fmt.Sprintf("%d 127.0.0.1:%d", port, tcpPort),
		"--DataDirectory", filepath.Join(dir, "data"),
		"--FascistFirewall", "1",
	)

	so, err := cmd.StdoutPipe()

	err = cmd.Start()
	if err != nil {
		_ = tl.Close()
		_ = os.RemoveAll(hsDir)
		return nil, err
	}

	readErr := make(chan error)
	lines := make(chan string)

	go func() {
		b := bufio.NewReader(so)
		for {
			s, err := b.ReadString('\n')
			if err != nil {
				readErr <- err
				return
			}
			lines <- s
		}
	}()

	t := time.NewTimer(timeout)

	msg1 := "Tor has successfully opened a circuit. Looks like client functionality is working."
	msg2 := "Bootstrapped 100%: Done"
	var done1, done2 bool
	for !done1 && !done2 {

		select {
		case <-t.C:
			return nil, ErrTimeout
		case err := <-readErr:
			return nil, err
		case s := <-lines:
			if strings.Contains(s, msg1) {
				done1 = true
			} else if strings.Contains(s, msg2) {
				done2 = true
			}
		}
	}

	return &Listener{address, port, l, cmd, dir}, nil
}

var _ net.Listener = &Listener{}

type Listener struct {
	address     string
	port        int
	tcpListener net.Listener
	cmd         *exec.Cmd
	tmpDir      string
}

func (l *Listener) Accept() (net.Conn, error) {
	return l.tcpListener.Accept()
}

func (l *Listener) Addr() net.Addr {
	return onionAddr{l.address, l.port}
}

type onionAddr struct {
	address string
	port    int
}

func (a onionAddr) String() string {
	return fmt.Sprintf("%s:%d", a.address, a.port)
}

func (a onionAddr) Network() string {
	return fmt.Sprintf("tor")
}

func (l *Listener) Close() error {
	err := l.tcpListener.Close()
	_ = l.cmd.Process.Kill()
	l.cmd.Wait()
	if l.tmpDir != "" {
		_ = os.RemoveAll(l.tmpDir)
	}
	return err
}

func rsaKeyToOnionAddress(rk rsa.PublicKey) (string, error) {
	type rsaPublicKey struct {
		N *big.Int
		E int
	}

	der, err := asn1.Marshal(rsaPublicKey{rk.N, rk.E})
	if err != nil {
		return "", err
	}
	h := sha1.New()
	if _, err := h.Write(der); err != nil {
		return "", err
	}
	sum := h.Sum(nil)

	str := base32.StdEncoding.EncodeToString(sum[0:10])
	return fmt.Sprintf("%s.onion", strings.ToLower(str)), nil
}
