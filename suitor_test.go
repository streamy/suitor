package suitor

import (
	"crypto/rand"
	"crypto/rsa"
	"fmt"
	"testing"
	"time"
)

// This doesn't really test very much right now.
func TestListen(t *testing.T) {
	priv, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		t.Fatal(err)
	}

	l, err := Listen(priv, 80, 30*time.Second)
	if err != nil {
		t.Fatal(err)
	}

	// will print "address is xo2jqubuexer7cta.onion:80" or similar
	fmt.Printf("address is %v\n", l.Addr())
	/*
		con, err := l.Accept()
		if err != nil {
			t.Fatal(err)
		}

		err = con.Close()
		if err != nil {
			t.Fatal(err)
		}
	*/
	err = l.Close()
	if err != nil {
		t.Fatal(err)
	}
}
